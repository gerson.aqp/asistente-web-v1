import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ListUsersComponent } from './components/admin/list-users/list-users.component';
import { ReportComponent } from './components/admin/report/report.component';
import { ProfileComponent } from './components/users/profile/profile.component';
import { TimeComponent } from './components/users/time/time.component';

const routes: Routes = [
	{
		path:'list-users',
		component: ListUsersComponent
	},
	{
		path:'report',
		component: ReportComponent
	},
	{
		path:'login',
		component: LoginComponent
	},
	{
		path:'home',
		component: HomeComponent
	},
	{
		path:'navbar',
		component: NavbarComponent
	},
	{
		path:'profile',
		component: ProfileComponent
	},
	{
		path:'time',
		component: TimeComponent
	}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
