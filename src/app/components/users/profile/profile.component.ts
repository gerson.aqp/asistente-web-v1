import { DatePipe } from '@angular/common';
import { User } from 'src/app/interfaces/user';

import { AuthService } from 'src/app/services/auth.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { UserService } from 'src/app/services/user.service';


@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css']
})

//Se lanza profile una vez se verifique el login
export class ProfileComponent implements OnInit {
	public isLogged: boolean = false;
	public isAdmin: boolean = false;
	public operation: string = "show";
	public userFirebase = firebase.auth().currentUser;
	public photoUrl: string;
	public user: User;
	public birthday: Date;
	public userAdm : User;


	constructor(private authService: AuthService, private usrservice: UserService, private datePipe: DatePipe) {
	}
	ngOnInit() {
		// this.getAdminVerfificador();
		this.getCurrentUser();
		this.LoginData();	
	}
	// getAdminVerfificador() {

	// }

	getCurrentUser() {
		this.authService.isAuth().subscribe(auth => {
			if (auth) {
				console.log('user logged');
				this.isLogged = true;
			} else {
				console.log('NOT user logged');
				this.isLogged = false;
			}
		});
	}
	//obtiene y asigna datos de usuario gmail, facebook para mostrarlos en el perfil
	async LoginData() {
		await this.userFirebase.providerData.forEach((profile => {
			this.user = {
				name: profile.displayName,
				email: profile.email,
				codFireBase: profile.uid,
				phoneNumber: profile.phoneNumber
			}
			//Esto sirve para obtener el tamaño deseado de la foto de facebook
			if (profile.uid.length == 17) {
				this.photoUrl = "https://graph.facebook.com/" + profile.uid + "/picture?height=500";
			}
			//Como las dimensiones de las otras fotos no tienen problemas solo se le asigna,
			else {
				this.photoUrl = profile.photoURL;
				//si la cuenta que ingreso no tiene una foto se asigna una por defecto
				if (this.photoUrl == null) {
					this.photoUrl = "https://pbs.twimg.com/profile_images/967417710995894272/_7L_wexv.jpg";
				}
			}

		}))
		this.authService.comprobarUserDB(this.user);
		await this.usrservice.getUser(this.user.codFireBase).subscribe(
			data => {
				this.userAdm = data;
				this.isAdmin = this.userAdm.personType;
				console.log("profileuser works"+this.userAdm.personType);
			}, error => {
				console.log(error);
			}
		);
		// this.authService.comprobarAdmin(this.user.codFireBase).subscribe(
		// 	data => {
		// 		this.userAdm = data;
		// 	}, error => {
		// 		console.log(error);
		// 	}
		// );
	}
	async updateBD() {
		this.user.birthdate = this.datePipe.transform(this.birthday, 'dd-MM-yyyy');
		console.log(this.user.birthdate);
		// this.user.birthdate=this.birthday.getDay+"-"+this.birthday.getMonth+"-"+this.birthday.getFullYear;
		// console.log(this.user.birthdate);
		this.authService.UpdateUserDB(this.user);
	}


}
