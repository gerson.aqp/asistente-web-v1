import { UserService } from 'src/app/services/user.service';
import { User } from 'src/app/interfaces/user';
import { AdminService } from './../../../services/admin.service';
import { Actividades } from './../../../interfaces/actividades';
import { Component, OnInit, SimpleChanges, OnChanges, DoCheck } from '@angular/core';
import { Break } from 'src/app/interfaces/break';
import * as firebase from 'firebase';
import { AuthService } from 'src/app/services/auth.service';

@Component({
	selector: 'app-time',
	templateUrl: './time.component.html',
	styleUrls: ['./time.component.css']
})
export class TimeComponent implements OnInit {
	public actividades: Actividades[];
	public breaks: Break[];
	public userFirebase = firebase.auth().currentUser;
	public userFB: User;
	public userDB: User;
	public index: number;
	public estado: boolean = false;
	public breakId1: number;
	public clicked = false;

	public isLogged: boolean = false;

	constructor(private authService: AuthService, private admservice: AdminService, private usrService: UserService) { }

	ngOnInit() {
		this.LoginData();
		this.getActividades_User();
		this.getBreaks_User();
		this.getUser();
		this.getCurrentUser();
	}

	getCurrentUser() {
		this.authService.isAuth().subscribe(auth => {
			if (auth) {
				console.log('user logged');
				this.isLogged = true;
			} else {
				console.log('NOT user logged');
				this.isLogged = false;
			}
		});
	}
	// ngOnChanges() {

	// }
	// OnChanges(index, name: string){
	// 	this.getBreaks_User();
	// 	this.break(index, name);
	// }
	getActividades_User() {
		this.admservice.getActividades().subscribe(
			data => {
				this.actividades = data;
			}
		);
	}
	async LoginData() {
		await this.userFirebase.providerData.forEach((profile => {
			this.userFB = {
				name: profile.displayName,
				email: profile.email,
				codFireBase: profile.uid,
				phoneNumber: profile.phoneNumber
			}
		}));

	}
	async getUser() {
		await this.usrService.getUser(this.userFB.codFireBase).subscribe(
			data => {
				this.userDB = data;
			}
		);
	}
	async getBreaks_User() {
		console.log(this.breakId1 + "");
		await this.usrService.getBreaks(this.userFB.codFireBase).subscribe(
			data => {
				this.breaks = data;
				this.breakId1 = this.breaks[(this.breaks.length) - 1].breakId;
				console.log(this.breakId1);
			}
		);

	}
	// al retornar la llamada habilitar los botones (pantalla de carga)
	async break(index, name: string) {
		// if (this.estado = false) {
		// 	this.getBreaks_User();
		// 	this.estado = true;
		await this.usrService.setOnBreak(this.userDB.personId, index, this.breaks[(this.breaks.length) - 1].active, this.breakId1, name);
		await this.getActividades_User();
		await this.getBreaks_User();
		// this.clicked=!this.clicked;
		// } else {
		// 	this.getBreaks_User();
		// 	this.estado = false;
		// 	this.usrService.setOffBreak(this.userDB.personId, index,this.breakId1, name);
		// }


	}
}
