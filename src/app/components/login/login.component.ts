import { AuthService } from './../../services/auth.service';

import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';


@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	constructor(private authenticationService: AuthService, private router: Router) { }
	public operation: string = 'login';
	public email: string = '';
	public password: string = '';

	ngOnInit() {
	}
	onRegister(): void {
		this.authenticationService.registerUser(this.email, this.password);
	}
	onLogin(): void {
		this.authenticationService.loginEmailUser(this.email, this.password);
	}

	onLoginGoogle(): void {
		this.authenticationService.loginGoogleUser();
	}
	onLoginFacebook(): void {
		this.authenticationService.loginFacebookUser();
	}
	onLogout() {
		this.authenticationService.logoutUser();
	}
}
