import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import * as firebase from 'firebase';
import { User } from 'src/app/interfaces/user';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
	public isLogged: boolean = false;
	public isAdmin: boolean = false;
	public userFirebase = firebase.auth().currentUser;
	// public user: User;
	constructor(private authService: AuthService) {
	}

	ngOnInit() {
		this.getCurrentUser();
		// this.comprobarAdm();
	}

	// async comprobarAdm() {
	// 	console.log("comprobar admin");
	// 	console.log("imprimiendo" + await this.authService.userAdmin.codFireBase);
	// }

	getCurrentUser() {
		this.authService.isAuth().subscribe(auth => {
			if (auth) {
				console.log('user logged');
				this.isLogged = true;
			} else {
				console.log('NOT user logged');
				this.isLogged = false;
			}
		});
	}
	onLogoutUser(): void {
		this.authService.logoutUser()
			.then((res) => {
				alert('hasta luego');
			}).catch(err => console.log('err', err.message));
	}
}
