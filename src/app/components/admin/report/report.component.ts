
import { Actividades } from './../../../interfaces/actividades';

import { AdminService } from './../../../services/admin.service';
import { Component, OnInit, DoCheck } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { registro } from 'src/app/interfaces/registro';
import { DatePipe } from '@angular/common';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import * as firebase from 'firebase';

@Component({
	selector: 'app-report',
	templateUrl: './report.component.html',
	styleUrls: ['./report.component.css']
})
//Componente para vista admin
export class ReportComponent implements OnInit {
	public users: User[];
	public idUser: string;
	public actividad: Actividades;
	public registroAll: registro[];
	public registroUser: registro;
	//conversion de datos fecha
	public dateFrom: Date;
	public dateFromStr: string;
	public dateTo: Date;
	public dateToStr: string;
	public today: Date;

	public isAdmin: boolean = false;
	public userFirebase = firebase.auth().currentUser;
	public user: User;
	public userAdm : User;

	constructor(private authService: AuthService, private usrservice: UserService,private admservice: AdminService, private datePipe: DatePipe) {
		this.idUser = null;
		this.dateFrom = new Date();
		this.dateTo = new Date();
		this.today = new Date();
	}
	ngOnInit() {
		this.getUsers_Admin();
		this.getReportAll_Admin();
		this.LoginData();
	}

	async LoginData() {
		await this.userFirebase.providerData.forEach((profile => {
			this.user = {
				name: profile.displayName,
				email: profile.email,
				codFireBase: profile.uid,
				phoneNumber: profile.phoneNumber
			}
		}))
		await this.usrservice.getUser(this.user.codFireBase).subscribe(
			data => {
				this.userAdm = data;
				this.isAdmin = this.userAdm.personType;
				console.log("list -user works "+this.isAdmin);
			}, error => {
				console.log(error);
			}
		);
	}
	//Se envia fechas desde y hasta cuando junto al id del usuario
	//usarlo cuando se solucione e problema del uso de uid
	getReportUser_Admin() {
		this.dateFromStr = this.datePipe.transform(this.dateFrom, 'dd-MM-yyyy');
		this.dateToStr = this.datePipe.transform(this.dateTo, 'dd-MM-yyyy');

		this.admservice.getRegistroUser(this.dateFromStr, this.dateToStr, this.idUser).subscribe(
			data => {
				this.registroUser = data;
			}, error => {
				alert("antes seleccionar usuario");
				console.log(error);
			}
		);
	}
	//Muestra el reporte actual antes de ingresar las fechas
	//id user si devuelve el id o tomar el index para sacar la info del arreglo
	getReportUser_Today(id) {
		this.idUser = id;
		console.log("reportuser" + this.idUser);
		this.admservice.getRegistroUser("01-05-2019", "12-06-2019", this.idUser).subscribe(
			data => {
				this.registroUser = data;
			}
		);
	}
	getReportAll_Admin() {
		this.admservice.getRegistroAll().subscribe(
			data => {
				this.registroAll = data;
			}
		);
	}
	deleteUser_Admin(id) {
		console.log("funciona");
		this.admservice.deleteEmployee(id).subscribe(
			() => console.log('Employee with ID ' + id + ' Deleted'),
			(err) => console.log(err)
		);

	}
	//Para la lista de usuarios (volverlo aponer)
	getUsers_Admin() {
		this.admservice.getUsers().subscribe(
			data => {
				this.users = data;
			}
		);
	}
	//una vez seleccionado se tendra los datos individuales de 
	//cada usuario(reporte de actividad y datos personales)
	// getUser_Admin(id:number) {
	// 	this.admservice.getUser(id).subscribe(
	// 		data=>{
	// 			this.user=data;
	// 		}
	// 	);
	// }
	// getActividad_Admin(id: number) {
	// 	this.admservice.getActividad(id).subscribe(
	// 		// Recibe el objeto data que en este caso es un objeto de tipo actividad
	// 		// asignamos los valores que se crearon para este servicio y deacuerdo a la interfaz
	// 		data => {
	// 			this.actividad = data;
	// 		}
	// 	);
	// }
}
