import { Actividades } from './../../../interfaces/actividades';
import { AdminService } from './../../../services/admin.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import * as firebase from 'firebase';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';

@Component({
	selector: 'app-list-users',
	templateUrl: './list-users.component.html',
	styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

	public actividades: Actividades[];
	public users: User[];
	public id: number;
	public name: string;
	public selectedUser: User;

	public isAdmin: boolean = false;
	public userFirebase = firebase.auth().currentUser;
	public user: User;
	public userAdm : User;
	
	constructor(private admservice: AdminService,private authService: AuthService, private usrservice: UserService,) { }

	ngOnInit() {
		this.getActividades_Admin();
		this.getUsers_Admin();
		this.LoginData();
	}

	async LoginData() {
		await this.userFirebase.providerData.forEach((profile => {
			this.user = {
				name: profile.displayName,
				email: profile.email,
				codFireBase: profile.uid,
				phoneNumber: profile.phoneNumber
			}
		}))
		await this.usrservice.getUser(this.user.codFireBase).subscribe(
			data => {
				this.userAdm = data;
				this.isAdmin = this.userAdm.personType;
				console.log("list -user works "+this.isAdmin);
			}, error => {
				console.log(error);
			}
		);
	}

	getUsers_Admin() {
		this.admservice.getUsers().subscribe(
			data => {
				this.users = data;
			}
		);
	}
	getActividades_Admin() {
		this.admservice.getActividades().subscribe(
			data => {
				this.actividades = data;
			}
		);
	}
	deleteUser_Admin(id) {
		console.log("funciona");
		this.admservice.deleteEmployee(id).subscribe(
			() => console.log('Usuario con ID ' + id + ' Eliminado'),
			(err) => console.log(err)
		);

	}
	deleteActividad_Admin(id) {
		console.log("funciona");
		this.admservice.deleteActividad(id).subscribe(
			() => console.log('Actividad con ID ' + id + ' Eliminado'),
			(err) => console.log(err)
		);

	}
	onSelect(userOn: User): void {
		this.selectedUser = userOn;
	}

	
}
