import { AdminService } from './services/admin.service';
import { UserService } from './services/user.service';
import {AuthService} from './services/auth.service';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReportComponent } from './components/admin/report/report.component';
import { ListUsersComponent } from './components/admin/list-users/list-users.component';
import { ProfileComponent } from './components/users/profile/profile.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';

import {FormsModule} from '@angular/forms';
import { AngularFireModule} from '@angular/fire';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { TimeComponent } from './components/users/time/time.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {DatePipe} from '@angular/common';



@NgModule({
  declarations: [
    AppComponent,
    ReportComponent,
    ListUsersComponent,
    ProfileComponent,
    HomeComponent,
    NavbarComponent,
    LoginComponent,
    TimeComponent  ],
  imports: [
    BrowserModule,
	AppRoutingModule,
	AngularFireModule.initializeApp(environment.config),
	AngularFirestoreModule,
    AngularFireAuthModule, 
	AngularFireDatabaseModule,
	FormsModule,
	ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
	HttpClientModule,
	BrowserModule,
	//environment.production ? ServiceWorkerModule.register('../dist/AttendanceAppWeb/ngsw-worker.js') : []
  ],
  providers: [
	  AuthService,
	  UserService,
	  AdminService,
	  DatePipe
	],
  bootstrap: [AppComponent]
})
export class AppModule { 
	
}
