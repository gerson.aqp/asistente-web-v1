import { Actividades } from './actividades';
import { DatePipe } from '@angular/common';
//En en back esta con nombre Break tenerlo en cuenta
export interface registro {
	id?: string;
	inicio?: Date;
	fin?: Date;
	fecha?: Date;
	userId?: number;
	actividadId?: number;
}
