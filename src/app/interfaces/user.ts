export interface User {
	personId?: number;
	name?: string;
	surName?:string;
	email?: string;
	identificationNumber?: string;
	codFireBase?: string;
	phoneNumber?: string;
	address?: string;
	personType?: boolean;
	birthdate?: string;
}
