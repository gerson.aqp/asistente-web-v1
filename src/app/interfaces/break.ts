export interface Break {
	breakId?: number;
    leewayId?: number;
    leewayName?: string;
    personId?: number;
    personName?: string;
    startTime?: string;
    endTime?: string;
    date?:  string;
    active?: boolean;
    total?: 0
}
