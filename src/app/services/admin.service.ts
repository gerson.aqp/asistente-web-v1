import { Actividades } from './../interfaces/actividades';
import { User } from './../interfaces/user';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { registro } from '../interfaces/registro';

@Injectable({
	providedIn: 'root'
})
export class AdminService {

	apiURL: string = 'http://192.168.1.100/attendanceapp/api';
	constructor(private http: HttpClient) { }
	//get user esta implementado en el servicio de usuario

	// public getUser(id:number) {
	// 	try {
	// 		return this.http.get<User>(this.apiURL + '/User/');
	// 	} catch (error) {
	// 		console.log(error);
	// 	}
	// }
	public getUsers() {
		try {
			return this.http.get<User[]>(this.apiURL + '/Person/GetAllPersons');
		} catch (error) {
			console.log(error);
		}
	}
	public getActividad(id: number) {
		try {
			return this.http.get<Actividades>(this.apiURL + '/Leeway/LeewayId/' + id);
		} catch (error) {
			console.log(error);
		}
	}
	public getActividades() {
		try {
			return this.http.get<Actividades[]>(this.apiURL + '/Leeway/GetAllLeeways');
		} catch (error) {
			console.log(error);
		}
	}
	public getRegistroUser(dateFrom: String, dateTo: String, userId: string) {
		try {
			return this.http.get<registro>(this.apiURL + '/Break/GetBreaksByDateFromtoDateToById/' + dateFrom + '/' + dateTo + '/' + userId);
		} catch (error) {
			console.log(error);
		}
	}
	public getRegistroAll() {
		try {
			return this.http.get<registro[]>(this.apiURL + '/Break/GetAllBreaks');
		} catch (error) {
			console.log(error);
		}
	}
	deleteEmployee(id: string) {
		try {
			return this.http.delete<User>(this.apiURL + '/Person/DeleteByCodFireBase/'+ id);
		} catch (error) {
			console.log(error);
		}
	}
	deleteActividad(id: string) {
		try {
			return this.http.delete<Actividades>(this.apiURL + '/Leeway/DeleteLeeway/' + id);
		} catch (error) {
			console.log(error);
		}
	}
	// public post(break:Breakmodel) {
	// 	try {
	// 		return this.http.post<registro>(this.apiURL + '/Break/GetAllBreaks',param);
	// 	} catch (error) {
	// 		console.log(error);
	// 	} 
	// }
}
