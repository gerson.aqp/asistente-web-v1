import { User } from 'src/app/interfaces/user';
import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { auth } from 'firebase/app';
import { Router, RouterLink } from '@angular/router';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';



@Injectable({
	providedIn: 'root'
})
export class AuthService {
	public admin: boolean = false;
	public userDB1 = false;
	apiURL: string = 'http://192.168.1.100/attendanceapp/api';
	public userFirebase = firebase.auth().currentUser;
	public user: User;

	constructor(public fireauth: AngularFireAuth, public firestore: AngularFirestore,
		public router: Router, public http: HttpClient) { }

	registerUser(email: string, pass: string) {
		try {
			return this.fireauth.auth.createUserWithEmailAndPassword(email, pass)
		} catch (e) {
			alert("Error!" + e.message);
		}
	}

	async loginEmailUser(email: string, pass: string) {
		try {
			await this.fireauth.auth.signInWithEmailAndPassword(email, pass);
			alert("Bienvenido");
			this.router.navigate(['/profile']);
		} catch (e) {
			alert("Error Login Email \n" + e.message);
		}
	}

	async loginFacebookUser() {
		try {
			await this.fireauth.auth.signInWithPopup(new auth.FacebookAuthProvider())
				.then(credential => this.updateUserData(credential.user));
			alert("Bienvenido");
			this.router.navigate(['/profile']);
		} catch (e) {
			alert("Error Login Facebook \n" + e.message);
		}
	}

	async loginGoogleUser() {
		try {
			await this.fireauth.auth.signInWithPopup(new auth.GoogleAuthProvider())
				.then(credential => this.updateUserData(credential.user));
			alert("Bienvenido");
			this.router.navigate(['/profile']);
		} catch (e) {
			alert("Error Login Google \n" + e.message);
		}
	}

	logoutUser() {
		try {
			return this.fireauth.auth.signOut();
			alert("hasta luego");
		} catch (e) {
			alert("Error al Salir\n" + e.message);
		}

	}

	isAuth() {
		return this.fireauth.authState.pipe(map(auth => auth));
	}

	// Verificador de logeo Firebase
	updateUserData(user) {
		const userRef: AngularFirestoreDocument<any> = this.firestore.doc('users/${user.uid}');
		const data: User = {
			codFireBase: user.uid,
			email: user.email
			//   roles: {
			// 	usuario: true
			//   }
		}
		return userRef.set(data, { merge: true })
	}
	//Comprueba el login del admin 

	async comprobarUserDB(user: User) {
		try {
			await this.http.get<boolean>(this.apiURL + '/Person/ValidatePersonByCodFireBase/' + user.codFireBase).subscribe(
				data => {
					this.userDB1 = data;
				});
		}
		catch (error) {
			console.log("Error\n" + error);
		}
		//si no encuentra al usuario en la bd hace el primer guardado enviando email y uid de firebase
		if (this.userDB1 == false) {
			this.firstsaveUserDB(user);
		}
		else {
			console.log("usuario ya registrado en la BD local\n" + user.email);
		}
	}
	//Primer logeo guardara email y uid en la BD
	async firstsaveUserDB(userFSU: User) {
		await this.http.post<any>(this.apiURL + '/Person/CreatePerson',
			{
				email: userFSU.email,
				codFireBase: userFSU.codFireBase
			}
		).subscribe(
			data => {
				console.log("POST Request is successful ", data);
			},
			error => {
				console.log("Error", error);
			});
	}

	//recibir uid y guardarlo en la bd junto al mail y password
	async UpdateUserDB(userUpdate: User) {
		await this.http.put<any>(this.apiURL + '/Person/UpdatePerson',
			{
				codFireBase: userUpdate.codFireBase,
				email: userUpdate.email,
				name: userUpdate.name,
				surName: userUpdate.surName,
				identificationNumber: userUpdate.identificationNumber,
				phoneNumber: userUpdate.phoneNumber,
				address: userUpdate.address,
				birthdate: userUpdate.birthdate,
				personType: false
			}).subscribe(
				data => {
					console.log("update Done ", data);
				},
				error => {
					console.log("Error", error);
				});
		alert("Guardado en la base local");
	}
}
