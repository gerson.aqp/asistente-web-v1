import { Break } from 'src/app/interfaces/break';
import { AngularFirestore } from '@angular/fire/firestore';

import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { HttpClient } from '@angular/common/http';
import * as firebase from 'firebase';
import { catchError } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	public breakVerificador: Break;
	apiURL: string = 'http://192.168.1.100/attendanceapp/api';
	//firestore para almacenar los fdatos en firestone pero se usara el
	// servidor local obteniendo datos directamente del login de facebook
	constructor(public firestore: AngularFirestore, private http: HttpClient) { }
	//   public getActividades() {
	// 	try {
	// 		return this.http.get<Actividades[]>(this.apiURL + '/Leeway/GetAllLeeways');
	// 	} catch (error) {
	// 		console.log(error);
	// 	}
	// }

	//obtendra lso datos del usuario seleccionado en list-users.ts para 
	public getUser(id: string) {
		try {
			return this.http.get<User>(this.apiURL + '/Person/Get1ByCodFireBase/' + id);
		} catch (error) {
			console.log(error);
		}
	}
	public getBreaks(cod: string) {
		try {
			// return this.http.get<registro>(this.apiURL + '/Break/DateFrom/' + dateFrom + '/DateTo/' + dateTo + '/PersonId/' + userId);
			return this.http.get<Break[]>(this.apiURL + '/Break/GetBreaksByDateFromtoDateToById/' + '05-05-2019/' + '12-12-2028/' + cod);
		} catch (error) {
			console.log(error);
		}
	}
	// agregara el break solo si el anterior no existe o si esta terminado 
	//y si no lo terminara para c¿iniciar otro 
	//console.log("ACTIVIDAD INICIADA",index, name);
	//aca se esta enviando id del leeway no del break
	async setOnBreak(userId: number, i: number, breakEstado: boolean,breakIdent:number, name: string) {
		console.log(breakIdent);
		if (breakIdent == null) {
			await this.http.post<Break>(this.apiURL + '/Break/CreateBreak',
				{
					personId: userId,
					leewayId: i
				}).subscribe(data => {
					console.log("Actividad confirmada ", name);
				}, error => {
					console.log("Error", error);
				});
		}
		else {
			//redundar 
			// await this.http.get<Break>(this.apiURL + '/Break/GetBreakById/' + (breaknum)).subscribe(
			// 	data => {
			// 		this.breakVerificador = data;
			// 	});

			if (breakEstado) {
				await this.http.put<Break>(this.apiURL + '/Break/UpdateBreak',
					{
						breakId: breakIdent,
						personId: userId,
						leewayId: i
					}).subscribe(data => {
						alert("Actividad detenida");
						console.log("Actividad detenida ", breakIdent, name);
					}, error => {
						console.log("Error", error);
					});
			}
			else {
				await this.http.post<Break>(this.apiURL + '/Break/CreateBreak',
					{
						personId: userId,
						leewayId: i
					}).subscribe(data => {
						alert("Actividad iniciada");
						console.log("Actividad confirmada ", breakIdent, name);
					}, error => {
						console.log("Error", error);
					});
			}
		}
	}
	async setOffBreak(userId: number, i: number, breaknum: number, name: string) {
		// await this.http.get<Break>(this.apiURL + '/Break/GetBreakById/' + (i - 1)).subscribe(
		// 	data => {
		// 		this.breakVerificador = data;
		// 	});
		// if (this.breakVerificador.active = false || this.breakVerificador == null) {
		this.http.put<Break>(this.apiURL + '/Break/UpdateBreak',
			{
				breakId: breaknum + 1,
				personId: userId,
				leewayId: i
			}).subscribe(data => {
				console.log("Actividad detenida ", breaknum, name);
			}, error => {
				console.log("Error", error);
			});
	}

}
