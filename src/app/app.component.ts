import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	title = 'AttendanceAppWeb';
	constructor(private swUpdate: SwUpdate) {
	}
	ngOnInit() {
		// Si existe una nueva version del programa este sera actualizado automaticamente, se 
		// puede ver las versiones de esta app en ngsw.json
		if(this.swUpdate.isEnabled) {

			this.swUpdate.available.subscribe(() => {

				if (confirm("Nueva version de la app \n¿Desea descargar la nueva version?")) {

					window.location.reload();
				}
			});
		}
	}

}
